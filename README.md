# Site Guardian User Status module

Site Guardian User Status adds some simple user statistics to the site Status Report.

It provides additional information to the Status report detailing some useful statistics and settings regarding user accounts of the Drupal system.

Values added are...

- Whether the admin user (user id 0) is enabled.
- The number of user accounts in the system.
- The number of accounts created last week, last month and last year.
- The number of accounts enabled/disabled.
- The number of accounts with the administrator role.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/sgd_user_status).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/sgd_user_status).

## Table of contents

- Recommended modules
- Installation
- Configuration
- Maintainers

## Recommended modules

Site Guardian User Status is a companion module for the Site Guardian (API) module and as such will add to the information returned to a consumer of the Site Guardian (API).

See the Site Guardian [project page](https://www.drupal.org/project/site_guardian).

If used in conjunction with the Site Guardian API it returns additional information when a consumer of the Site Guardian (API) requests the sites information.

At present the module return statistics on the roles and the number of user accounts using each role.

Although part of the Site Guardian ecosystem it provides functionality that is useful irrespective of whether the Site Guardian (API) is installed or not and does not require the Site Guardian (API) module to function.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will add information to the Site Status report as described above.

## Maintainers

- Andy Jones - [arcaic](https://www.drupal.org/u/arcaic)
