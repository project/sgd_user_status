<?php

/**
 * @file
 * Install file for sgd_user_status.
 */

/**
 * Implements hook_requirements().
 *
 * Returns additional information about users.
 *
 * - Total number of accounts
 * - Number of enabled/disabled accounts
 * - User 1 status (enabled/disabled)
 *
 * - Number of accounts used within the year
 * - Number of accounts unused for over a year
 * - Number of accounts never used
 *
 * - Number of accounts created in last week
 * - Number of accounts created in last month
 * - Number of accounts created in last year
 *
 * - Users with admin role
 */

use Drupal\user\Entity\Role;

/**
 * Implements hook_requirements().
 */
function sgd_user_status_requirements($phase) {

  // If this is anything other than reporting then just return.
  if ($phase !== 'runtime') {
    return [];
  }

  // DB Connection.
  $database = \Drupal::database();

  $requirements = [];

  // Get the total number of user accounts.
  $query = $database->select('users', 'u');

  $userAccountCount = $query->countQuery()->execute()->fetchField();

  // Get number of enabled and disabled accounts.
  $query = $database->select('users_field_data', 'ufd');
  $query->fields('ufd', ['status']);
  $query->addExpression('count(ufd.status)', 'count');
  $query->groupBy('status');

  $result = $query->execute();

  $countData = $result->fetchAllAssoc('status', \PDO::FETCH_ASSOC);

  $requirements['sgd_user_count'] = [
    'title' => t('Users - Count'),
    'value' => $userAccountCount,
    'description' => t('Number of user accounts.'),
    'severity' => REQUIREMENT_INFO,
  ];

  $requirements['sgd_user_status'] = [
    'title' => t('Users - Status'),
    'value' => $countData[1]['count'] . '/' . $countData[0]['count'],
    'description' => t('User account status (enabled/disabled).'),
    'severity' => REQUIREMENT_INFO,
  ];

  // Get Status of User 1 (Admin user)
  $query = $database->select('users_field_data', 'ufd');
  $query->fields('ufd', ['uid', 'status']);
  $query->condition('uid', 1);

  $result = $query->execute();

  $statusData = $result->fetchAllAssoc('uid', \PDO::FETCH_ASSOC);

  $requirements['sgd_user_1_status'] = [
    'title' => t('Users - Admin status'),
    'value' => $statusData[1]['status'] ? t('Enabled') : t('Disabled'),
    'description' => t('Status of the admin account.'),
    'severity' => REQUIREMENT_INFO,
  ];

  // User Account activity stats.
  $dateTime1WeekAgo = strtotime("-1 week", time());
  $dateTime1MonthAgo = strtotime("-1 month", time());
  $dateTime1YearAgo = strtotime("-1 year", time());

  $query = $database->select('users_field_data', 'ufd');

  $query->addExpression('SUM(CASE WHEN ufd.access > 0 AND ufd.access >  ' . $dateTime1YearAgo . ' THEN 1 ELSE 0 END)', 'active');
  $query->addExpression('SUM(CASE WHEN ufd.access > 0 AND ufd.access <= ' . $dateTime1YearAgo . ' THEN 1 ELSE 0 END)', 'inactive');
  $query->addExpression('SUM(CASE WHEN ufd.access = 0 THEN 1 ELSE 0 END)', 'never');

  $query->addExpression('SUM(CASE WHEN ufd.created > ' . $dateTime1WeekAgo . ' THEN 1 ELSE 0 END)', 'createdWeek');
  $query->addExpression('SUM(CASE WHEN ufd.created > ' . $dateTime1MonthAgo . ' THEN 1 ELSE 0 END)', 'createdMonth');
  $query->addExpression('SUM(CASE WHEN ufd.created > ' . $dateTime1YearAgo . ' THEN 1 ELSE 0 END)', 'createdYear');

  $result = $query->execute();

  $activityData = $result->fetchAll(\PDO::FETCH_ASSOC);

  $requirements['sgd_user_activity'] = [
    'title' => t('Users - Activity'),
    'value' => $activityData[0]['active'] . '/' . $activityData[0]['inactive'] . '/' . $activityData[0]['never'],
    'description' => t('User activity (Recently/Within 1 Year/Never).'),
    'severity' => REQUIREMENT_INFO,
  ];

  $requirements['sgd_user_creation'] = [
    'title' => t('Users - Created'),
    'value' => $activityData[0]['createdWeek'] . '/' . $activityData[0]['createdMonth'] . '/' . $activityData[0]['createdYear'],
    'description' => t('User creation (Last week/Last month/Last year).'),
    'severity' => REQUIREMENT_INFO,
  ];

  // If we have an administrator role.
  if (Role::load('administrator')) {

    // Get all user with the role.
    $query = $database->select('user__roles', 'ur');
    $query->condition('roles_target_id', 'administrator');
    $adminRoleCount = $query->countQuery()->execute()->fetchField();

    $requirements['sgd_user_admin_count'] = [
      'title' => t('Users - with admin role'),
      'value' => $adminRoleCount,
      'description' => t("Number of users with the 'administrator' role."),
      'severity' => REQUIREMENT_INFO,
    ];
  }

  return $requirements;
}
